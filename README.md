# Bio Bokeh

```sh
$ python3 -m venv .venv
```

```sh
$ source .venv/bin/activate
```

```sh
bokeh serve --dev --show line-chart.py
bokeh serve --dev --show basic/sorted.py
```


https://docs.bokeh.org/en/2.4.3/docs/user_guide/server.html
https://www.datacamp.com/tutorial/tutorial-bokeh-web-app-deployment
